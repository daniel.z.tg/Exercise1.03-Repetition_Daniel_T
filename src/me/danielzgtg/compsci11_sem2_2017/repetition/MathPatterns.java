package me.danielzgtg.compsci11_sem2_2017.repetition;

/**
 * A program for displaying the beauty of math, using loops
 * 
 * @author Daniel Tang
 * @since 14 February 2017, with methods 21 February 2017
 */
public final class MathPatterns {

	/**
	 * The layout for the first pattern
	 */
	private static final String OUTPUT_LAYOUT_1 =
			"%9d x 8 + %d = %-9d\n";

	/**
	 * The layout for the second pattern
	 */
	private static final String OUTPUT_LAYOUT_2 =
			"%9d x 9 + %d%s= %-10d\n";

	/**
	 * The layout for the third pattern
	 */
	private static final String OUTPUT_LAYOUT_3 =
			"%9d x 9 + %d = %-10d\n";

	/**
	 * The layout for the fourth pattern
	 */
	private static final String OUTPUT_LAYOUT_4 =
			"%s%d x %d =%s%d\n";

	/**
	 * A lot of spaces. Substringed for lack of better way to add spaces.
	 */
	private static final String SPACES = "                   ";

	public static void main(final String[] ignore) {
		showPattern1();
		showPattern2();
		showPattern3();
		showPattern4();
	}

	/**
	 * Generates and Prints the first pattern to the console.
	 */
	private static final void showPattern1() {
		// Common re-used variable for factors
		long factor = 0;

		// 1 x 8 + 2 = 9
		// 12 x 8 + 2 = 98
		// 123 x 8 + 2 = 987
		// Pattern: concat one more than last digit, and one less than last digit,
		//          on first factor, and product (respectively).
		for (int i = 1; i <= 9; i++) {
			factor = factor * 10 + i; // Shift previous digits left, and add greater digits each time.

			System.out.format(OUTPUT_LAYOUT_1,
					factor, // 1, 12, 123, etc
					// "x 8" is in layout
					i, // 1, 2, 3, etc
					factor * 8 + i); // 9, 98, 987, etc
		}
	}

	/**
	 * Generates and Prints the second pattern to the console.
	 */
	private static final void showPattern2() {
		long factor = 0;
		// 1 x 9 + 2 = 11
		// 12 x 9 + 3 = 111
		// 123 x 9 + 4 = 1111
		// Pattern: concat one more than last digit on first factor,
		//          increase addend by 1 each time,
		//          and product consists of addend number of "1" digits.
		for (int i = 1; i <= 9; i++) {
			factor = factor * 10 + i; // Shift previous digits left, and add greater digits each time.

			System.out.format(OUTPUT_LAYOUT_2,
					factor, // 1, 12, 123, etc
					// "x 9" is in layout
					i + 1, // 2, 3, 4, etc
					/*
					 * 1 to 9 are one digit numbers
					 * 10 is a two digit numbers
					 * This removes the space when it is 10
					 * to keep the rest aligned
					 */
					i + 1 < 10 ? " " : "",
							factor * 9 + (i + 1)); // 11, 111, 1111, etc
		}
	}

	/**
	 * Generates and Prints the third pattern to the console.
	 */
	private static final void showPattern3() {
		long factor = 0;
		// 9 x 9 + 7 = 88
		// 98 x 9 + 6 = 888
		// 987 x 9 + 5 = 8888
		// Pattern: concat one less than last digit on first factor,
		//          decrease addend by 1 each time,
		//          and product consists of one more "8" digit each time.
		for (int i = 7; i >= 0; i--) {
			// Shift previous digits left, and add less digits each time.
			// (loop counts down, so decreasing)
			factor = factor * 10 + (i + 2);

			System.out.format(OUTPUT_LAYOUT_3,
					factor, // 9, 98, 987, etc
					// "x 9" is is layout
					i, // 7, 6, 5, etc
					factor * 9 + i); // 88, 888, 8888, etc
		}
	}

	/**
	 * Generates and Prints the fourth pattern to the console.
	 */
	private static final void showPattern4() {
		long factor = 0;
		// 1 x 1 = 1
		// 11 x 11 = 121
		// 111 x 111 = 12321
		// Pattern: factor are the same, and consist of row number of "1" digits,
		//          product is row number surrounded by all previous digits until "1".
		for (int i = 0; i < 9; i++) {
			factor = factor * 10 + 1; // Shift previous digits left, and add another "1" digit

			System.out.format(OUTPUT_LAYOUT_4,
					SPACES.substring(0,
							i < 7 ? // Put in the proper amount of spaces
									(6 - i) * 2 : // One line, and less indents until 7 is in the middle
										15 - i), // Then two lines, alot of spaces, but still decreases
					factor, factor, // 1 x 1, 11 x 11, 111 x 111, etc
					i < 7 ? // Determines line breaking or not
							" " : // One space after equal sign and no new line until after 7
								"\n" + SPACES.substring(0, 18 - i), // New line, and similar indents after
								factor * factor); // 1, 121, 12321, etc
		}
	}

	private MathPatterns() { throw new UnsupportedOperationException(); }
}
