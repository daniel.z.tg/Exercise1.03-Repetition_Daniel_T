package me.danielzgtg.compsci11_sem2_2017.repetition;

import java.util.Scanner;

/**
 * A program for calculating half of an even number.
 * 
 * @author Daniel Tang
 * @since 13 Febuary 2017
 */
public final class HalfNumber {

	/**
	 * Prompt layout for positive even number input.
	 */
	private static final String EVEN_NUMBER_PROMPT = "Please enter a positive even number: ";

	/**
	 * The error message when the user did not enter a proper integer.
	 */
	private static final String ERR_MSG_NOT_INT =
			"Sorry! I didn't understand that integer, please try again.";

	/**
	 * The error message when the user did not enter a positive integer.
	 */
	private static final String ERR_MSG_NOT_POSITIVE =
			"Sorry! That integer is not positive, please try again.";

	/**
	 * The error message when the user did not enter an even integer.
	 */
	private static final String ERR_MSG_NOT_EVEN =
			"Sorry! That integer is not even, please try again.";

	/**
	 * Message outputted after a good number is entered.
	 */
	private static final String NUMBER_ENTERED_MSG =
			"You have entered an even number.";

	/**
	 * The output layout for the calculated result
	 */
	private static final String OUTPUT_LAYOUT =
			"Half of [%d] is [%d]\n";

	public static void main(final String[] ignore) {
		final int number = promptForEvenInt();

		// Thank the user for their number.
		System.out.println(NUMBER_ENTERED_MSG);
		// Tell them the result of the calculation
		System.out.format(OUTPUT_LAYOUT, number, number / 2);
	}

	/**
	 * Obtains an positive even number.
	 * 
	 * @return The positive even number as an {@code int}.
	 */
	private static int promptForEvenInt() {
		/*variable*/ String userInput;
		try (final Scanner scanner = new Scanner(System.in)) {
			while (true) {
				// Prompt the user and get the input
				System.out.print(EVEN_NUMBER_PROMPT);
				userInput = scanner.nextLine();

				try {
					// Try to get an integer
					final int result = Integer.valueOf(userInput);

					// Stop asking only if it is positive and even
					// or complain and try again
					if (result < 0) {
						System.out.println(ERR_MSG_NOT_POSITIVE);
						continue;
					}

					if (result % 2 != 0) {
						System.out.println(ERR_MSG_NOT_EVEN);
						continue;
					}

					return result;
				} catch (final NumberFormatException nfe) {
					// Keep asking until it is a valid integer
					System.out.println(ERR_MSG_NOT_INT);
				}
			}
		}
	}

	private HalfNumber() { throw new UnsupportedOperationException(); }
}
